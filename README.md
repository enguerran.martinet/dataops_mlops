# DataOps_MLOps

*Premier pas sur git et gitlab*

__Je fais un README pas très joli__

## Petit Hello world
```
int main()
{
    printf("Hello world!\n");
    return 0;
}
```
J'ai trouvé comment mettre un lien en markdown avec cette page : [documentation de Framasite](https://docs.framasoft.org/fr/grav/) !

## Petit Tableau

| Aligné à gauche  | Centré          | Aligné à droite |
| :--------------- |:---------------:| -----:|
| Aligné à gauche  |   ce texte        |  Aligné à droite |
| Aligné à gauche  | est centré             |   Aligné à droite |

